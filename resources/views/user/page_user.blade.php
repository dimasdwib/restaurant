@extends('master/master')

@section('title')
  <i class="fa fa-fw fa-users"></i> User Management
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="panel my-panel">
      <div class="panel-body">
        <div class="form-group">
          <button class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> Tambah User </button>
        </div>
        <div class="table-responsive">
          <table class="table table-condensed table-striped">
            <thead>
              <tr>
                <th> No </th>
                <th> Nama </th>
                <th> Username </th>
                <th> </th>
              </tr>
            </thead>
            <tbody>
            @if (count($user) > 0)
              @foreach ($user as $key => $item)
              <tr>
                <td> {{ $key + 1 }} </td>
                <td> {{ $item->name }} </td>
                <td> {{ $item->username }} </td>
                <td>
                  <a class="btn btn-primary btn-xs"><i class="fa fa-edit fa-fw"></i> Edit </a>
                  &nbsp;
                  @if ($item->id_user != 1)
                    <a class="btn btn-danger btn-xs"><i class="fa fa-trash fa-fw"></i> Hapus </a>
                  @endif
                </td>
              </tr>
              @endforeach
            @else
              <tr>
                <td> Tidak ada user </td>
              </tr>
            @endif
            </tbody>
          </tabel>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
