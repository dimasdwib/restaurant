@extends('master/master')

@section('title')
  <i class="fa fa-fw fa-cog"></i> Setting
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="panel my-panel">
      <div class="panel-body">
        <form method="POST" action="{{ route('admin.update_setting') }}">
        <input type="hidden" value="{{ csrf_token() }}" name="_token" />
        <div class="form-group">
          <label> Full Name </label>
          <input type="text" value="{{ $user->name }}" name="name" required class="form-control" placeholder="Full Name" />
        </div>
        <div class="form-group">
          <label> Username </label>          
          <input type="text" value="{{ $user->username }}" name="username" required class="form-control" placeholder="Username" />
        </div>
        <div class="form-group">
          <label> Password </label>                    
          <input type="password" value="" class="form-control" placeholder="Password" />   
          <small> Kosongkan jika tidak mengganti password</small>     
        </div>
        <div class="form-group">
          <button class="btn btn-primary"> Update </button>        
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
