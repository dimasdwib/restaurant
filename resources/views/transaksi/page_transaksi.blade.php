@extends('master/master')

@section('title')
  <i class="fa fa-fw fa-shopping-basket"></i> Pemesanan
@endsection

@section('content')

<div class="row">
    <div class="col-md-7 col-sm-6">
      <div class="panel my-panel">
        <div class="panel-heading">
          <i class="fa fa-fw fa-lg fa-cutlery"></i> Menu
        </div>
        <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <select name="id_kategori" class="form-control">
                    <option value="0"> Semua Kategori </option>
                    @if (count($kategori) > 0)
                      @foreach ($kategori as $k)
                        <option value="{{ $k->id_kategori }}"> {{ $k->nama_kategori }} </option>
                      @endforeach
                    @endif
                  </select>
                </div>
              </div>
              <div class="col-md-8">
                <form id="form-cari">
                <div class="input-group form-group">
                    <input class="form-control" name="cari" placeholder="Cari Menu.." />
                  <span class="input-group-btn">
                    <button id="btn-cari" type="button" onclick="cari()" class="btn btn-primary"><i class="fa fa-search fa-fixed"></i> Cari </button>
                  </span>
                </div>
                </form>
              </div>
            </div>

          <hr>
          <div class="row" id="list-menu">
            @if (count($foods) > 0)
              @foreach ($foods as $item)
                <div class="col-md-3 col-sm-6 col-xs-6"  title="{{ $item->nama_produk }}">
                  <a class="panel-produk" onclick="add_item({{$item->id_produk}}, '{{$item->nama_produk}}', {{$item->harga_produk}})">
                  <div class="panel panel-default">
                    <div class="panel-img">
                      <img src="{{ url('img/'.$item->image) }}" />
                    </div>
                    <div class="panel-body">
                      <span class="panel-title"> {{ strlen($item->nama_produk) > 15 ? substr($item->nama_produk, 0, 13).' ..' : $item->nama_produk }} </span>
                      <span class="panel-price"> {{ $item->label_harga }} </span>
                    </div>
                  </div>
                  </a>
                </div>
              @endforeach
            @else
              <h4 class="text-center text-grey"> Produk tidak ditemukan </h4>
            @endif
          </div>
        </div>
        </div>
      </div>
    <div class="col-md-5 col-sm-6">
      <div class="panel my-panel">
        <div class="panel-heading">
          <i class="fa fa-fw fa-lg fa-file-text-o"></i> Rincian Transaksi
        </div>
        <div class="panel-body">
          <table class="table table-condensed table-striped">
            <thead>
              <tr>
                <th></th>
                <th class="text-center"> Desc </th>
                <th class="text-center"> Harga </th>
                <th class="text-center"> Qty </th>
                <th class="text-center"> Sub total </th>
              </tr>
            </thead>
            <tbody id="cart-item">
            </tbody>
            <tfoot>
              <tr id="total-item" style="display:none">
                <td colspan="4" class="text-right"><b> TOTAL </b></td>
                <td class="text-right total-item" style="color:#dd2b13;font-weight:600;font-size:1.5em"> 0 </td>
              </tr>
            </tfoot>
          </table>
          <textarea style="display:none" id="data-item">[]</textarea>
          <button id="btn-save" style="display:none" onclick="pembayaran()" class="btn btn-primary btn-block btn-lg"> Pembayaran </button>
        </div>
      </div>
    </div>
  </div>

<div id="modal-confirm" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="panel my-panel" style="padding:0; margin: 0;">
          <div class="panel-heading">
            <i class="fa fa-tags fa-file-text-o fa-fixed"></i> Pembayaran
          </div>
          <div class="panel-body">
            <h2 class="text-center" style="color: red" id="total-bayar"></h2>
            <hr />
            <div class="form-group">
              <div class="row">
                <div class="col-md-3 text-right"> <b style="font-size:1.2em">Jumlah Bayar</b> </div>
                <div class="col-md-9">
                  <input placeholder="Jumlah Bayar" class="form-control input-lg" onkeyup="hitung_kembali()" id="jumlah-bayar" value="" type="number" />
                  <span id="error-bayar" class="text-danger" style="display:none"> <i class="fa fa-exclamation-circle fa-fixed"></i> Pembayaran masih kurang </span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-3 text-right"> <b style="font-size:1.2em">Kembalian</b> </div>
                <div class="col-md-9">
                  <input readonly id="jumlah-kembali" class="form-control input-lg" value="0" type="number" />
                </div>
              </div>
            </div>
            <hr />
            <div class="text-right">
              <button type="button" id="btn-bayar" class="btn btn-primary btn-lg" onclick="submit_transaksi()"><i class="fa fa-check-circle fa-fixed"></i> Bayar</button>
              &nbsp;
              <button type="button" id="btn-cancel" class="btn btn-default btn-lg" data-dismiss="modal">Batal</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">

$(document).ready(function() {
  hitung_total();
  $("#form-cari").submit(function(e) {
    e.preventDefault();
    cari();
  });
});

function cari() {
  var url = "{{ url('/') }}";
  var kat = $("[name='id_kategori']").val();
  var key = $("[name='cari']").val();
  $.ajax({
    url: "{{ route('admin.cari_menu') }}",
    method: 'post',
    data: { _token: "{{ csrf_token() }}", id_kategori: kat, key: key },
    beforeSend: function() {
      $("#list-menu").html("");
      $("#list-menu").append('<h4 class="text-center text-grey"><i class="fa fa-spinner fa-pulse fa-fw fa-lg"></i> Loading </h4>');
    }
  })
  .done(function(res) {
    $("#list-menu").html("");
    if (res.menu.length > 0) {
      $.map(res.menu, function(item) {
        html = '<div class="col-md-3 col-sm-6 col-xs-6" title="'+item.nama_produk+'">'+
          '<a class="panel-produk" onclick="add_item('+item.id_produk+', \''+item.nama_produk+'\', '+item.harga_produk+')">'+
          '<div class="panel panel-default">'+
            '<div class="panel-img">'+
              '<img src="'+url+'/img/'+item.image+'" />'+
            '</div>'+
            '<div class="panel-body">'+
              '<span class="panel-title">'+((item.nama_produk.length > 15) ? item.nama_produk.substr(0, 13) + '..' : item.nama_produk) +'</span>'+
              '<span class="panel-price">'+item.label_harga+'</span>'+
            '</div>'+
          '</div>'+
          '</a>'+
        '</div>';
        $("#list-menu").hide().append(html).show('fast');
      });
    } else {
      $("#list-menu").append('<h4 class="text-center text-grey"><i class="fa fa-fw fa-meh-o fa-lg"></i> Produk tidak ditemukan </h4>');
    }
  });
}

function findWithAttr(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

function money(int) {
  return 'Rp. '+ int;
}

function add_item(id, nama, harga) {
  $("#item-norecord").remove();
  var append = true;
  var qty = 1;
  var subtotal = Number(harga) * qty;
  var data_item = JSON.parse($("#data-item").val());
  if ($("#item-"+id+" .item-qty").length > 0) {
    append = false;
    data_item = JSON.parse($("#data-item").val());
    qty = Number($("#item-"+id+" .item-qty").val()) + 1;
    $("#item-"+id+" .item-qty").val(qty);

    subtotal = Number(harga) * qty;
    $("#item-"+id+" .item-subtotal").html(subtotal);

    var index = findWithAttr(data_item, 'id_produk', id);
    data_item[index] = {
      id_produk: id,
      harga_produk: harga,
      qty: qty,
      subtotal: subtotal
    };
    $("#data-item").val(JSON.stringify(data_item));
  }

  if (append) {
    var html = '<tr id="item-'+id+'">'+
      '<td><a style="color:#a13e54;cursor:pointer" onclick="remove_item('+id+')"><i class="fa fa-times-circle fa-lg"></i></a></td>'+
      '<td>'+nama+'</td>'+
      '<td class="text-center item-harga" data-harga="'+harga+'">'+harga+'</td>'+
      '<td class="text-right">'+
        '<input style="width:70px" onchange="change_qty_item('+id+')" class="form-control input-xs item-qty" type="number" min="1" value="'+qty+'" />'+
      '</td>'+
      '<td class="text-right item-subtotal">'+subtotal+'</td>'+
    '</tr>';
    $("#cart-item").append(html);
    data_item.push({
      id_produk: id,
      harga_produk: harga,
      qty: qty,
      subtotal: subtotal
    });
    $("#data-item").val(JSON.stringify(data_item));
    snackbar("<i class='fa fa-check-circle fa-fw fa-lg'></i> "+nama+ " berhasil ditambah");
  }

  hitung_total();

}

function change_qty_item(id) {
  if ($("#item-"+id+" .item-qty").length > 0) {
    var qty = $("#item-"+id+" .item-qty").val();
    if (!$.isNumeric(Number(qty)) || Number(qty) < 1) {
      alert('Invalid value');
      return;
    }
    var harga = $("#item-"+id+" .item-harga").attr('data-harga');
    var subtotal = Number(harga) * Number(qty);
    $("#item-"+id+" .item-subtotal").html(subtotal);

    var data_item = JSON.parse($("#data-item").val());
    var index = findWithAttr(data_item, 'id_produk', id);
    data_item[index] = {
      id_produk: id,
      harga_produk: harga,
      qty: qty,
      subtotal: subtotal
    };
    $("#data-item").val(JSON.stringify(data_item));

    hitung_total();
  }
}

function remove_item(id) {
  $("#item-"+id).remove();
  data_item = JSON.parse($("#data-item").val());
  var index = findWithAttr(data_item, 'id_produk', id);
  data_item.splice(index, 1);
  $("#data-item").val(JSON.stringify(data_item));
  hitung_total();
}

function hitung_total() {
  var total = 0;
  var item = 0;
  $("#cart-item tr").map(function (i, d) {
    var qty = $(d).find(".item-qty").val();
    var harga = $(d).find(".item-harga").attr('data-harga');
    var subtotal = Number(harga) * Number(qty);
    total = total + subtotal;
    item = item + 1;
  });
  $("#total-item .total-item").html(total);
  if (item == 0) {
    html = '<tr id="item-norecord"><td colspan="5" class="text-center text-grey"><br><i class="fa fa-3x fa-cutlery"></i><h4>Silakan pilih menu</4></td></tr>';
    $("#cart-item").html(html);
    $("#btn-save").hide('fast');
    $("#total-item").hide('fast');
  } else {
    $("#btn-save").show('fast');
    $("#total-item").show('fast');
  }
}

function pembayaran() {
  var total = 0;
  $("#cart-item tr").map(function (i, d) {
    var qty = $(d).find(".item-qty").val();
    var harga = $(d).find(".item-harga").attr('data-harga');
    var subtotal = Number(harga) * Number(qty);
    total = total + subtotal;
  });

  $("#jumlah-bayar").val(0);
  $("#jumlah-kembali").val(0);
  $("#modal-confirm").modal('toggle');
  $("#total-bayar").html("Total pembayaran : Rp "+total);
}

function submit_transaksi() {
  var data_item = JSON.parse($("#data-item").val());
  var total = 0
  data_item.map(function(d, i) {
    total = total + Number(d.subtotal);
    console.log(d);
  });
  var bayar = Number($("#jumlah-bayar").val());
  var kembali = bayar - total;

  console.log(bayar, total);
  if (bayar < total) {
    $("#error-bayar").show('fast');
    return false;
  }

  if(!confirm("Simpan Transaksi ?")) {
    return false;
  }

  var item = $("#data-item").val();

  $.ajax({
    url: "{{ url('/admin/transaksi/submit') }}",
    data: { total: total, item: item, _token: '{{ csrf_token() }}' },
    type: "POST",
    beforeSend: function() {
      $("#btn-bayar").attr('disabled', true);
      $("#btn-cancel").attr('disabled', true);
      $("#btn-bayar").html('<i class="fa fa-spinner fa-pulse fa-fw fa-lg"></i> Loading');
    }
  }).done(function(res) {
    $("#btn-bayar").html('<i class="fa fa-check-circle fa-fw fa-lg"></i> Success');
    snackbar("<i class='fa fa-check-circle fa-fw fa-lg'></i> Transaksi berhasil ditambah");
    alert(res.message);
    window.location.reload();
  }).fail(function(err) {
    console.log(err);
  });
}

function hitung_kembali() {
  var total = 0;
  var item = 0;
  var kembali = 0;
  $("#cart-item tr").map(function (i, d) {
    var qty = $(d).find(".item-qty").val();
    var harga = $(d).find(".item-harga").attr('data-harga');
    var subtotal = Number(harga) * Number(qty);
    total = total + subtotal;
    item = item + 1;
  });

  var bayar = Number($("#jumlah-bayar").val());
  if (!$.isNumeric(bayar)) {
    alert("invalid value");
    return false;
  }

  if (bayar >= total) {
    kembali = bayar - total;
    $("#jumlah-kembali").val(kembali);
    $("#error-bayar").hide('fast');
  } else {
    $("#jumlah-kembali").val(0);
  }
}

</script>
@endsection
