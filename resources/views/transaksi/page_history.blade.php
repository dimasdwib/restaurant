@extends('master/master')

@section('title')
  <i class="fa fa-fw fa-file-text-o"></i> History Transaksi
@endsection

@section('content')
<div class="row">
  <div class="col-md-4">
    <form>
    <div class="form-group input-group">
      <input type="text" placeholder="Pencarian" name="cari" class="form-control" value="{{ isset($_GET['cari']) ? $_GET['cari'] : null }}" />
      <span class="input-group-btn">
        <button class="btn btn-primary"> <i class="fa fa-search fa-fw"></i> Cari </button>
      </span>
    </div>
    </form>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel my-panel">
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-condensed table-striped">
            <thead>
              <tr>
                <th> No Transaksi</th>
                <th> Kasir </th>
                <th> Tanggal / Jam </th>
                <th> Total </th>
                <th> </th>
              </tr>
            </thead>
            <tbody>
            @if (count($transaksi) > 0)
              @foreach ($transaksi as $item)
              <tr>
                <td> {{ $item->no_transaksi }} </td>
                <td> {{ $item->name }} </td>
                <td> {{ $item->label_tanggal }} </td>
                <td> {{ $item->label_total }} </td>
                <td>
                  <a onclick="show_detail({{$item->id_transaksi}})" class="btn btn-primary btn-xs"><i class="fa fa-file-text-o fa-fw"></i> Detail </a>
                </td>
              </tr>
              @endforeach
            @else
              <tr>
                <td> Tidak ada transaksi </td>
              </tr>
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modal-detail" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 id="modal-title">Detail Transaksi</h3>
        <h5 id="modal-date">Tangal : </h5>
        <h5 id="modal-user">Kasir : </h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="panel my-panel">
              <div class="panel-heading"> Detail Transaksi </div>
              <div class="panel-body">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th> Item </th>
                      <th> Qty </th>
                      <th> Harga </th>
                      <th> Subtotal </th>
                    </tr>
                  </thead>
                  <tbody id="table-detail">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="text-right">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@section('javascript')
<script type="text/javascript">

function show_detail(id_transaksi) {
  $.ajax({
    url: "{{ route('admin.detail_transaksi') }}",
    method: 'POST',
    data: { id_transaksi: id_transaksi, _token: "{{ csrf_token() }}" }
  })
  .done(function(res) {
    $("#table-detail").html("");
    $.each(res.detail, function(i, d) {
      var tr = "<tr><td>"+ d.nama_produk +"</td><td>"+ d.qty +"</td><td>"+ d.label_harga +"</td><td>" + d.label_subtotal + "</td></tr>";
      $("#table-detail").append(tr);
    });
    var tr = "<tr><td colspan='3' class='text-right'><h4>TOTAL</h4></td><td><h4>"+ res.transaksi.label_total +"</h4></td></tr>";
    $("#table-detail").append(tr);

    $("#modal-title").html("<i class='fa fa-fw fa-file-text-o'></i> Detail Transaksi " + res.transaksi.no_transaksi);
    $("#modal-date").html("&nbsp;<i class='fa fa-fw fa-calendar'></i> Tanggal : " + res.transaksi.label_tanggal);
    $("#modal-user").html("&nbsp;<i class='fa fa-fw fa-user'></i> User : " + res.transaksi.name);
  })
  .fail(function(err) {
    console.log(err);
  });

  $("#modal-detail").modal('show');
}

</script>
@endsection
