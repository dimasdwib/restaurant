<!DOCTYPE html>
<html>
  <head>
    <title> Restaurant </title>
    <link rel="stylesheet" href="{!! url('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}" type="text/css" />
    <link rel="stylesheet" href="{!! url('bower_components/bootstrap/dist/css/bootstrap-theme.min.css') !!}" type="text/css" />
    <link rel="stylesheet" href="{!! url('bower_components/components-font-awesome/css/font-awesome.min.css') !!}" type="text/css" />
    <script type="text/javascript" src="{!! url('bower_components/jquery/dist/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! url('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>

    <link rel="stylesheet" href="{!! url('assets/css/style.css') !!}" type="text/css" />
    @yield('head')

  </head>
  <body>
    <div class="topbar">
      <span class="brand-text"> <i class="fa fa-fw fa-cutlery"></i> Restaurant Foodie</span>
    </div>
    <div class="wrapper">
      <div class="sidenav">
        <ul>
          <li><a href="{{ route('admin.analisa') }}"><i class="fa fa-fw fa-line-chart"></i> Analisa </a></li>
          <li><a href="{{ route('admin.transaksi') }}"><i class="fa fa-fw fa-shopping-basket"></i> Pemesanan </a></li>
          <li><a href="{{ route('admin.history') }}"><i class="fa fa-fw fa-file-text-o"></i> History </a></li>
          <li><a href="{{ route('admin.produk') }}"><i class="fa fa-fw fa-tags"></i> Menu </a></li>
          <li><a href="{{ route('admin.setting') }}"><i class="fa fa-fw fa-cog"></i> Setting </a></li>
          <li><a href="{{ route('admin.logout') }}"><i class="fa fa-fw fa-power-off"></i> Logout </a></li>
        </ul>
      </div>
      <div class="contentwrap">
        <div class="headerwrap">
          @yield('title')
        </div>
        <div class="maincontent">
          @if (\Session::has('success'))
              <div class="alert alert-success">
                {!! \Session::get('success') !!}
              </div>
          @endif
          @if (\Session::has('error'))
              <div class="alert alert-danger">
                {!! \Session::get('error') !!}
              </div>
          @endif
          @yield('content')
          <div class="snackbar" id="snackbar">
            Notif Snackabar
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    function snackbar(text, time) {
      $("#snackbar").html(text);
      $("#snackbar").addClass('snackshow');
      setTimeout(function() {
        $("#snackbar").removeClass('snackshow');
      }, time ? time : 2000);
    }
    </script>
    @yield('javascript')
  </body>
</html>
