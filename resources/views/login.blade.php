<!DOCTYPE html>
<html>
<head>
    <title> Login </title>
    <link rel="stylesheet" href="{!! url('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}" type="text/css" />
    <link rel="stylesheet" href="{!! url('bower_components/bootstrap/dist/css/bootstrap-theme.min.css') !!}" type="text/css" />
<style>
body {
  background-color: #f2f2f2;
  background-image: url('bg1.jpg');
  background-size: cover;
  background-attachment: fixed;
}

.btn-primary {
  /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff670f+0,fc2e0f+100;Orange+Flat+%231 */
  background: #ff670f; /* Old browsers */
  background: -moz-linear-gradient(top, #ff670f 0%, #fc2e0f 100%); /* FF3.6-15 */
  background: -webkit-linear-gradient(top, #ff670f 0%,#fc2e0f 100%); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(to bottom, #ff670f 0%,#fc2e0f 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff670f', endColorstr='#fc2e0f',GradientType=0 ); /* IE6-9 */

  border: 1px solid #fe5b27;
}

.btn-primary:hover, .btn-primary:active, .btn-primary:focus {
  border: 1px solid #fe5b27;
  /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff823a+1,f93920+100 */
background: #ff823a; /* Old browsers */
background: -moz-linear-gradient(top, #ff823a 1%, #f93920 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #ff823a 1%,#f93920 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #ff823a 1%,#f93920 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff823a', endColorstr='#f93920',GradientType=0 ); /* IE6-9 */
}
</style>
</head>
<body>
  <div class="container">
    <div class="row" style="padding-top: 10%">
      <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
          <div class="panel-body">
            <h2 class="text-center" style="margin:0;padding:0"> Login </h2>
            <hr />
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                  {!! \Session::get('error') !!}
                </div>
            @endif
            <form action="{{ route('auth.submit_login') }}" method="POST">
              <input type="hidden" value="{{ csrf_token() }}" name="_token" />
              <div class="form-group">
                <input type="text" name="username" class="form-control input-lg" placeholder="Username" />
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control input-lg" placeholder="Password" />
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-lg"> Login </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
