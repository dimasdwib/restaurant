@extends('master/master')

@section('title')
  <i class="fa fa-line-chart fa-fw"></i> Analisa
@endsection

@section('head')
<link rel="stylesheet" href="{{ url('assets/chartist/chartist.css') }}" type="text/css" />
@endsection

@section('content')
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      <form class="form-inline">
        <div class="">
          <select class="form-control input-lg" name="chart">
            <option value="penjualan"> Penjualan </option>
            <option value="harga"> Harga </option>
          </select>
          <select class="form-control input-lg" name="kategori">
            <option value=""> Semua </option>
            @foreach($kategori as $k)
              <option value="{{ $k->id_kategori }}"> {{ $k->nama_kategori }}</option>
            @endforeach
          </select>
          <button class="btn btn-primary btn-lg"> OK </button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
  $page = isset($_GET['chart']) ? $_GET['chart'] : 'penjualan';
?>
@if($page == 'harga')
  @include('analisa/chart_harga', ['data' => $data]);
@else
  @include('analisa/chart_penjualan', ['data' => $data]);
@endif
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function() {
  $("[name='chart']").val("<?= isset($_GET['chart']) ? $_GET['chart'] : 'penjualan' ?>");
  $("[name='kategori']").val("<?= isset($_GET['kategori']) ? $_GET['kategori'] : null ?>");
});
</script>
@endsection
