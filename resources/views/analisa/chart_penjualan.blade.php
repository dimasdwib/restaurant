<div class="row">
  <div class="col-md-12">
    <div class="panel my-panel">
      <div class="panel-heading">
        <i class="fa fa-line-chart fa-fw fa-lg"></i> Analisa Penjualan
      </div>
      <div class="panel-body" style="max-height:400px">
        <div class="ct-chart ct-perfect-fourth"></div>
      </div>
    </div>
  </div>
</div>
<script src="{{ url('assets/chartist/chartist.js') }}"></script>
<script type="text/javascript">
var datalist = {!! $data !!};
var labels = [];
var series_makan = [];
var series_minum = [];

Object.keys(datalist).forEach(function(key) {
  labels.push(datalist[key].date);
  series_makan.push(datalist[key].jumlah_makan);
  series_minum.push(datalist[key].jumlah_minum);
});

var data = {
  // A labels array that can contain any sort of values
  labels: labels,
  // Our series array that contains series objects or in this case series data arrays
  @if(isset($_GET['kategori']) && $_GET['kategori'] != '')
    @if($_GET['kategori'] == 1)
      series : [
        series_makan
      ]
    @else
      series : [
        series_minum
      ]
    @endif
  @else
    series: [
      series_makan,
      series_minum,
    ]
  @endif
};

var options = {
  height: 380,
  showArea: true,
  axisY: {
    offset: 110,
    labelInterpolationFnc: function(value) {
      return 'Rp. '+ value
    },
    onlyInteger: true,
  }
}

new Chartist.Line('.ct-chart', data, options);
</script>
