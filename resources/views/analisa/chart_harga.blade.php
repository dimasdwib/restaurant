<div class="row">
  <div class="col-md-12">
    <div class="panel my-panel">
      <div class="panel-heading">
        <i class="fa fa-line-chart fa-fw fa-lg"></i> Analisa Harga
      </div>
      <div class="panel-body">
        <div class="ct-chart"></div>
      </div>
    </div>
  </div>
</div>
<script src="{{ url('assets/chartist/chartist.js') }}"></script>
<script type="text/javascript">

var datalist = {!! $data !!};
var labels = [];
var series = [];
var max = 0;
Object.keys(datalist).forEach(function(key) {
  labels.push(datalist[key].produk);
  series.push(Number(datalist[key].harga_produk));
  if (Number(datalist[key].harga_produk) > max) {
    max = Number(datalist[key].harga_produk);
  }
});

var data = {
  // A labels array that can contain any sort of values
  labels: labels,
  // Our series array that contains series objects or in this case series data arrays
  series: [
    series,
  ]
};

var option = {
  height: 500,
  horizontalBars: true,
  axisY: {
    offset: 130
  },
  axisX : {
    labelInterpolationFnc: function(value) {
      return value
    },
    low: 0,
    high: max,
    scaleMinSpace: 50,
    onlyInteger: true,
    offset: 170,
  }
}
new Chartist.Bar('.ct-chart', data, option);
</script>
