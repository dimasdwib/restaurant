@extends('master/master')

@section('title')
  <i class="fa fa-tags fa-fw"></i> Produk
@endsection

@section('content')
<div class="row">
  <form>
  <div class="col-md-2">
    <div class="form-group">
      <select name="kategori" class="form-control">
        <option value="0"> Semua </option>
        @foreach($kategori as $item)
          <option value="{{ $item->id_kategori }}"> {{ $item->nama_kategori }} </option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group input-group">
      <input type="search" name="cari" value="<?= isset($_GET['cari']) ? $_GET['cari'] : null ?>" placeholder="Pencarian" class="form-control" />
      <span class="input-group-btn">
        <button class="btn btn-primary"><i class="fa fa-search"></i> Cari </button>
      </span>
    </div>
  </div>
  </form>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel my-panel">
      <div class="panel-body">
        <div class="form-group">
          <button class="btn btn-primary" id="btn-add"><i class="fa fa-plus-circle fa-fw"></i> Tambah Menu </button>
        </div>
        <div class="table-responsive">
          <table class="table table-condensed table-striped">
            <thead>
              <tr>
                <th> No </th>
                <th> Image </th>
                <th> Nama </th>
                <th> Kategori </th>
                <th> Harga </th>
                <th> </th>
              </tr>
            </thead>
            <tbody>
            @if (count($produk) > 0)
              @foreach ($produk as $key => $item)
              <tr id="produk-{{ $item->id_produk }}"
                data-idproduk="{{ $item->id_produk }}"
                data-idkategori="{{ $item->id_kategori }}"
                data-nama="{{ $item->nama_produk }}"
                data-harga="{{ $item->harga_produk }}"
                data-image="{{ $item->image != null ? url('img/'.$item->image) : null }}"
              >
                <td> {{ $key + 1 }} </td>
                <td> <img style="max-width:80px;" src="{{ url('img/'.$item->image) }}" /> </td>
                <td> {{ $item->nama_produk }} </td>
                <td> {{ $item->nama_kategori }} </td>
                <td> {{ $item->label_harga }} </td>
                <td>
                  <a onclick="edit_produk({{ $item->id_produk }})" class="btn btn-primary btn-xs"> <i class="fa fa-edit fa-fw"></i> Edit </a>
                  &nbsp;
                  <form style="display: inline" action="{{ route('admin.hapus_produk') }}" method="POST">
                    <input hidden name="_token" value="{{ csrf_token() }}" />
                    <input hidden name="id_produk" value="{{ $item->id_produk }}" />
                    <button type="submit" onclick="return confirm('Anda yakin ingin hapus menu ?')" class="btn btn-default btn-xs"> <i class="fa fa-trash fa-fw"></i> Hapus </button>
                  </form>
                </td>
              </tr>
              @endforeach
            @else
              <tr>
                <td> Tidak ada transaksi </td>
              </tr>
            @endif
            </tbody>
          </tabel>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modal-produk" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
      <div class="panel my-panel">
        <div class="panel-heading"><i class="fa fa-fw fa-tag"></i><span id="modal-title">Tambah Menu</span></div>
        <div class="panel-body">
          <form method="POST" enctype="multipart/form-data" action="{{ route('admin.submit_produk') }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
          <input type="hidden" name="id_produk" />
          <div class="form-group">
            <label class="control-label "> Kategori </label>
            <div >
            <select name="id_kategori" class="form-control">
              @foreach($kategori as $item)
                <option value="{{ $item->id_kategori }}"> {{ $item->nama_kategori }} </option>
              @endforeach
            </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label"> Nama </label>
            <div class="">
              <input name="nama" class="form-control" placeholder="Nama Menu" />
            </div>
          </div>
          <div class="form-group">
            <label class="control-label"> Harga </label>
            <div class="">
              <input name="harga" class="form-control" placeholder="Harga" />
            </div>
          </div>
          <div class="form-group">
            <div>
              <div id="image"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label"> Image </label>
            <div class="">
              <input type="file" name="image" class="form-control" />
              <small id="notes" class="text-danger"> Kosongkan jika tidak ingin mengganti gambar </small>
            </div>
          </div>
          <div class="text-right">
            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle fa-fw"></i><span id="btn-submit"> Tambah </span> </button>
              &nbsp;
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          </form>
        </div>
      </div>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@section('javascript')
<script type="text/javascript">

$("#btn-add").on('click', function() {
  $("#image").html("");
  $("#notes").hide();
  $("#modal-title").html("Tambah Menu");
  $("#btn-submit").html("Tambah");
  $("[name='id_produk']").val("");
  $("#modal-produk").modal('show');
});

function edit_produk(id_produk) {
  $("#modal-title").html("Edit Menu");
  $("#btn-submit").html("Update");
  var data = $("#produk-"+id_produk);
  $("#modal-produk").modal('show');
  $("[name='id_produk']").val(data.attr('data-idproduk'));
  $("[name='id_kategori']").val(data.attr('data-idkategori'));
  $("[name='nama']").val(data.attr('data-nama'));
  $("[name='harga']").val(data.attr('data-harga'));

  if (data.attr('data-image') != '') {
    var image = '<br /><img style="max-width:250px" src="'+data.attr('data-image')+'" />';
    $("#image").html(image);
    $("#notes").show();
  } else {
    $("#image").html("");
    $("#notes").hide();
  }
}

</script>
@endsection
