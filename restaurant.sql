-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2018 at 05:54 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `ms_kategori`
--

CREATE TABLE `ms_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_kategori`
--

INSERT INTO `ms_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Makanan'),
(2, 'Minuman');

-- --------------------------------------------------------

--
-- Table structure for table `ms_produk`
--

CREATE TABLE `ms_produk` (
  `id_produk` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `harga_produk` decimal(15,2) NOT NULL DEFAULT '0.00',
  `nama_produk` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_produk`
--

INSERT INTO `ms_produk` (`id_produk`, `id_kategori`, `harga_produk`, `nama_produk`, `image`) VALUES
(1, 1, '18000.00', 'Burger', 'beefburger.jpg'),
(2, 1, '26000.00', 'Nasi Goreng', 'nasigoreng.jpg'),
(3, 1, '29000.00', 'Mie Goreng', 'miegoreng.jpg'),
(4, 2, '5000.00', 'Es Teh', 'esteh.png'),
(5, 1, '20000.00', 'Ayam Geprek', 'ayamgeprek.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `total`, `tanggal`) VALUES
(2, 1, '52000.0000', '2018-01-05 17:00:13'),
(3, 1, '34000.0000', '2018-01-06 17:02:57'),
(4, 1, '31000.0000', '2018-01-02 17:03:50'),
(5, 1, '52000.0000', '2018-01-06 17:05:07'),
(6, 1, '34000.0000', '2018-01-06 17:19:28'),
(7, 1, '34000.0000', '2018-01-07 19:47:31'),
(8, 1, '43000.0000', '2018-01-08 02:52:27'),
(9, 1, '44000.0000', '2018-01-08 02:54:29'),
(10, 1, '98000.0000', '2018-01-09 01:32:37'),
(11, 1, '60000.0000', '2018-01-09 01:36:07'),
(12, 1, '73000.0000', '2018-01-09 01:36:41'),
(13, 1, '98000.0000', '2018-01-09 01:37:31'),
(14, 1, '49000.0000', '2018-01-09 01:38:35'),
(15, 1, '78000.0000', '2018-01-09 01:39:46'),
(16, 1, '60000.0000', '2018-01-09 02:20:24'),
(17, 1, '60000.0000', '2018-01-09 02:41:36'),
(18, 1, '34000.0000', '2018-01-09 02:43:17'),
(19, 1, '72000.0000', '2018-01-09 03:08:19'),
(20, 1, '60000.0000', '2018-01-09 03:09:57');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_detail`
--

CREATE TABLE `transaksi_detail` (
  `id_transaksi_detail` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` decimal(15,4) NOT NULL,
  `subtotal` decimal(15,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_detail`
--

INSERT INTO `transaksi_detail` (`id_transaksi_detail`, `id_transaksi`, `id_produk`, `qty`, `harga`, `subtotal`) VALUES
(1, 2, 3, 1, '29000.0000', '29000.0000'),
(2, 2, 1, 1, '18000.0000', '18000.0000'),
(3, 2, 4, 1, '5000.0000', '5000.0000'),
(4, 3, 4, 1, '5000.0000', '5000.0000'),
(5, 3, 3, 1, '29000.0000', '29000.0000'),
(6, 4, 2, 1, '26000.0000', '26000.0000'),
(7, 4, 4, 1, '5000.0000', '5000.0000'),
(8, 5, 3, 1, '29000.0000', '29000.0000'),
(9, 5, 4, 1, '5000.0000', '5000.0000'),
(10, 5, 1, 1, '18000.0000', '18000.0000'),
(11, 6, 3, 1, '29000.0000', '29000.0000'),
(12, 6, 4, 1, '5000.0000', '5000.0000'),
(13, 7, 4, 1, '5000.0000', '5000.0000'),
(14, 7, 3, 1, '29000.0000', '29000.0000'),
(15, 8, 1, 1, '18000.0000', '18000.0000'),
(16, 8, 5, 1, '20000.0000', '20000.0000'),
(17, 8, 4, 1, '5000.0000', '5000.0000'),
(18, 9, 1, 1, '18000.0000', '18000.0000'),
(19, 9, 2, 1, '26000.0000', '26000.0000'),
(20, 10, 4, 1, '5000.0000', '5000.0000'),
(21, 10, 2, 1, '26000.0000', '26000.0000'),
(22, 10, 1, 1, '18000.0000', '18000.0000'),
(23, 10, 5, 1, '20000.0000', '20000.0000'),
(24, 10, 3, 1, '29000.0000', '29000.0000'),
(25, 11, 3, 1, '29000.0000', '29000.0000'),
(26, 11, 4, 1, '5000.0000', '5000.0000'),
(27, 11, 2, 1, '26000.0000', '26000.0000'),
(28, 12, 1, 1, '18000.0000', '18000.0000'),
(29, 12, 2, 1, '26000.0000', '26000.0000'),
(30, 12, 3, 1, '29000.0000', '29000.0000'),
(31, 13, 1, 1, '18000.0000', '18000.0000'),
(32, 13, 2, 1, '26000.0000', '26000.0000'),
(33, 13, 3, 1, '29000.0000', '29000.0000'),
(34, 13, 4, 1, '5000.0000', '5000.0000'),
(35, 13, 5, 1, '20000.0000', '20000.0000'),
(36, 14, 4, 1, '5000.0000', '5000.0000'),
(37, 14, 2, 1, '26000.0000', '26000.0000'),
(38, 14, 1, 1, '18000.0000', '18000.0000'),
(39, 15, 1, 1, '18000.0000', '18000.0000'),
(40, 15, 3, 1, '29000.0000', '29000.0000'),
(41, 15, 4, 1, '5000.0000', '5000.0000'),
(42, 15, 2, 1, '26000.0000', '26000.0000'),
(43, 16, 4, 1, '5000.0000', '5000.0000'),
(44, 16, 3, 1, '29000.0000', '29000.0000'),
(45, 16, 2, 1, '26000.0000', '26000.0000'),
(46, 17, 2, 1, '26000.0000', '26000.0000'),
(47, 17, 3, 1, '29000.0000', '29000.0000'),
(48, 17, 4, 1, '5000.0000', '5000.0000'),
(49, 18, 4, 1, '5000.0000', '5000.0000'),
(50, 18, 3, 1, '29000.0000', '29000.0000'),
(51, 19, 3, 1, '29000.0000', '29000.0000'),
(52, 19, 1, 1, '18000.0000', '18000.0000'),
(53, 19, 5, 1, '20000.0000', '20000.0000'),
(54, 19, 4, 1, '5000.0000', '5000.0000'),
(55, 20, 3, 1, '29000.0000', '29000.0000'),
(56, 20, 4, 1, '5000.0000', '5000.0000'),
(57, 20, 2, 1, '26000.0000', '26000.0000'),
(58, 21, 3, 1, '29000.0000', '29000.0000'),
(59, 21, 7, 1, '25000.0000', '25000.0000'),
(60, 21, 6, 1, '260000.0000', '260000.0000'),
(61, 21, 9, 1, '23000.0000', '23000.0000'),
(62, 22, 1, 2, '18000.0000', '36000.0000'),
(63, 22, 10, 2, '150000.0000', '300000.0000');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `remember_token` varchar(64) NULL	
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `name`, `email`, `password`, `remember_token`) VALUES
(1, 'administrator', 'Administrator', 'dimasdwib@gmail.com', '$2y$10$jvpXxieUSkTp1PWbtO23VuPFhsACviqJw0LOG2FuTC.7cCqRE7vGy', 'aksjndhcthnskclpdou873j2n1kls9');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ms_kategori`
--
ALTER TABLE `ms_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `ms_produk`
--
ALTER TABLE `ms_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`id_transaksi_detail`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ms_kategori`
--
ALTER TABLE `ms_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_produk`
--
ALTER TABLE `ms_produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `id_transaksi_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
