<?php
namespace App\Http\Controllers\Transaksi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
class TransaksiController extends Controller {

  function page_transaksi() {
    
    $foods = DB::table('ms_produk')
      ->join('ms_kategori', 'ms_produk.id_kategori', '=', 'ms_kategori.id_kategori')
      ->get();

    $kategori = DB::table('ms_kategori')
                ->get();

    if (count($foods) > 0) {
      foreach ($foods as $key => $value) {
        $foods[$key]->label_harga = $this->money($value->harga_produk);
      }
    }

    return view('transaksi/page_transaksi', [
      'foods' => $foods,
      'kategori' => $kategori,
    ]);
  }

  function cari_menu(Request $request) {
    $id_kategori = $request->input('id_kategori');
    $key = $request->input('key');
    $menu = DB::table('ms_produk')
      ->join('ms_kategori', 'ms_produk.id_kategori', '=', 'ms_kategori.id_kategori');

    if ($id_kategori != 0) {
      $menu->where('ms_kategori.id_kategori', '=', $id_kategori);
    }
    if ($key != '') {
      $menu->whereRaw("(ms_kategori.nama_kategori LIKE '%$key%' OR nama_produk LIKE '%$key%')");
    }
    $menu = $menu->get();

    if (count($menu) > 0) {
      foreach ($menu as $key => $value) {
        $menu[$key]->label_harga = $this->money($value->harga_produk);
      }
    }

    return response()->json([
      'menu' => $menu,
    ]);
  }

  function money($int) {
    return 'Rp '.number_format($int, 0, ',', '.');
  }

  function no_transaksi($id_transaksi) {
    $no_trans = '000000000';
    $c = strlen($id_transaksi);
    $no_trans = substr($no_trans, 0, strlen($no_trans) - $c);
    return $no_trans.$id_transaksi;
  }

  function page_history(Request $request) {
    $cari = $request->input('cari');
    $transaksi = DB::table('transaksi')
                    ->leftJoin('user', 'user.id_user', '=', 'transaksi.id_user')
                    ->orderBy('tanggal', 'DESC');

    if ($cari != '') {
      $transaksi->whereRaw("(transaksi.id_transaksi LIKE '%$cari%') OR (user.name LIKE '%$cari%')");
    }
    $transaksi = $transaksi->paginate(20);

    if (count($transaksi) > 0) {
      foreach ($transaksi as $key => $value) {
        $transaksi[$key]->no_transaksi = $this->no_transaksi($value->id_transaksi);
        $transaksi[$key]->label_total = $this->money($value->total);
        $transaksi[$key]->label_tanggal = date('d F Y H:i:s', strtotime($value->tanggal));
      }
    }
    return view('transaksi/page_history', [
      'transaksi' => $transaksi,
    ]);

  }

  function submit_transaksi(Request $request) {
    $item = $request->input('item');
    $total = $request->input('total');

    DB::beginTransaction();

    /* Insert Transaski */
    $id_transaksi = DB::table('transaksi')
      ->insertGetId([
        'id_user' => 1, // admin
        'total' => $total,
        'tanggal' => date('Y-m-d H:i:s')
      ]);

    /* Insert Detail Transaksi */
    $items = json_decode($item);
    $transaksi_detail = [];
    foreach ($items as $key => $item) {
      $transaksi_detail[] = [
        'id_transaksi' => $id_transaksi,
        'id_produk' => $item->id_produk,
        'qty' => $item->qty,
        'harga' => $item->harga_produk,
        'subtotal' => $item->subtotal,
      ];
    }

    $insert_detail = DB::table('transaksi_detail')
                      ->insert($transaksi_detail);

    if ($id_transaksi && $insert_detail) {
      DB::commit();
      return response()->json([
        'status' => 'success',
        'message' => 'Transaksi berhasil disimpan',
      ]);
    } else {
      DB::rollBack();
      return response()->json([
        'status' => 'failed',
        'message' => 'Transaksi gagal disimpan',
      ]);
    }
  }

  function detail_transaksi(Request $request) {
    $id_transaksi = $request->input('id_transaksi');
    $transaksi = DB::table('transaksi')
                  ->select('transaksi.*', 'name')
                  ->leftJoin('user', 'user.id_user', '=', 'transaksi.id_user')
                  ->where('id_transaksi', $id_transaksi)
                  ->first();

    $detail = DB::table('transaksi_detail')
      ->leftJoin('ms_produk', 'ms_produk.id_produk', '=', 'transaksi_detail.id_produk')
      ->where('id_transaksi', $id_transaksi)
      ->get();

    if (count($detail) > 0) {
      foreach ($detail as $key => $value) {
        $detail[$key]->label_harga = $this->money($value->harga);
        $detail[$key]->label_subtotal = $this->money($value->subtotal);
      }
    }

    $transaksi->no_transaksi = $this->no_transaksi($transaksi->id_transaksi);
    $transaksi->label_total = $this->money($transaksi->total);
    $transaksi->label_tanggal = date('m F Y H:i:s', strtotime($transaksi->tanggal));

    return response()->json([
      'detail' => $detail,
      'transaksi' => $transaksi,
    ]);
  }

}
