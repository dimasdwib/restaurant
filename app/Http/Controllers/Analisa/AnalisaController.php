<?php
namespace App\Http\Controllers\Analisa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class AnalisaController extends Controller {

  function page_analisa(Request $request) {

    if ($request->input('chart') == 'harga') {
      $data = json_encode($this->harga($request));
    } else {
      $data = json_encode($this->penjualan($request));
    }

    $kategori = DB::table('ms_kategori')
                  ->get();

    return view('analisa/page_analisa', [
      'data' => $data,
      'kategori' => $kategori
    ]);
  }

  function penjualan($request) {
    $now = date('m');
    $date_now = (int) date('d');
    $transaksi = DB::table('transaksi')
                    ->select(DB::raw("transaksi.*, transaksi_detail.harga * transaksi_detail.qty as total_harga, ms_produk.id_kategori"))
                    ->join('transaksi_detail', 'transaksi_detail.id_transaksi', '=', 'transaksi.id_transaksi')
                    ->join('ms_produk', 'transaksi_detail.id_produk', '=', 'ms_produk.id_produk')
                    ->whereRaw("MONTH(tanggal) = $now");

    $transaksi = $transaksi->get();

    for ($i = 1; $i <= date('t'); $i++) {
      $data[$i] = [
        'date' => $i,
      ];
      if ($i <= $date_now) {
        $data[$i]['jumlah_makan'] = 0;
        $data[$i]['jumlah_minum'] = 0;
      }
    }

    if (count($transaksi) > 0) {
      foreach ($transaksi as $key => $value) {
        $d = (int) date('d', strtotime($value->tanggal));
        if (isset($data[$d])) {
          if ($value->id_kategori == 2) {
            $data[$d]['jumlah_minum'] += $value->total_harga;
          } else {
            $data[$d]['jumlah_makan'] += $value->total_harga;
          }
        }
      }
    }

    return $data;
  }

  function harga($request) {
    $now = (int) date('m');
    $produk = DB::table('transaksi_detail')
                  ->select(DB::raw("transaksi_detail.id_produk, nama_produk, sum(subtotal) as harga_produk, sum(qty) as total_qty"))
                  ->join('transaksi', 'transaksi.id_transaksi', '=', 'transaksi_detail.id_transaksi')
                  ->join('ms_produk', 'ms_produk.id_produk', '=', 'transaksi_detail.id_produk')
                  ->whereRaw("(MONTH(tanggal) = $now)");


    if ($request->input('kategori') != '') {
      $produk->where('ms_produk.id_kategori', $request->input('kategori'));
    }

    $produk = $produk->groupBy('transaksi_detail.id_produk')
                    ->get();
    $data = [];
    if (count($produk) > 0) {
      foreach ($produk as $key => $value) {
        $data[$key]['harga_produk'] = $value->harga_produk;
        $data[$key]['total_qty'] = $value->total_qty;
        $data[$key]['produk'] = strlen($value->nama_produk) > 15 ? substr($value->nama_produk, 0, 15). ' ..' : $value->nama_produk;
      }
    }

    return $data;
  }

}
