<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Hash;

class UserController extends Controller {

  function page_user() {
    $user = DB::table('user')
              ->paginate(20);
              
    return view('user/page_user', [
      'user' => $user,
    ]);
  }

  function login(Request $request) {
    $username = $request->input('username');
    $password = $request->input('password');
    
    return redirect()->route('admin.transaksi');
  }

  function setting() {
    $user = Auth::user();
    return view('user/setting', ['user' => $user]);
  }

  function update_setting(Request $request) {
    $user = [
      'name' => $request->name,
      'username' => $request->username,
    ];
  
    if ($request->password != null) {
      $user['password'] = Hash::make($request->password);
    }
  
    $update = DB::table('user')
                ->where('id_user', Auth::id())
                ->update($user);
  
    if ($update) {
      return redirect()->route('admin.setting')->with('success', 'Data berhasil diperbarui');
    } else {
      return redirect()->route('admin.setting')->with('error', 'Data gagal diperbarui');      
    }
  }

  function logout() {
    Auth::logout();
    return redirect()->route('login');
  }

}
