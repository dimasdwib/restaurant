<?php
namespace App\Http\Controllers\Produk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ProdukController extends Controller {

  function page_produk(Request $request) {
    $kategori = $request->input('kategori');
    $cari = $request->input('cari');

    $produk = DB::table('ms_produk')
                ->select('ms_produk.*', 'ms_kategori.nama_kategori')
                ->join('ms_kategori', 'ms_kategori.id_kategori', '=', 'ms_produk.id_kategori');

    if ($kategori != 0) {
      $produk->where('ms_produk.id_kategori', $kategori);
    }

    if ($cari != '') {
      $produk->whereRaw("(nama_produk LIKE '%$cari%' OR nama_kategori LIKE '%$cari%')");
    }

    $produk = $produk->paginate(20);
    $kategori = DB::table('ms_kategori')
                ->get();

    if (count($produk) > 0) {
        foreach ($produk as $key => $value) {
          $produk[$key]->label_harga = $this->money($value->harga_produk);
        }
    }

    return view('produk/page_produk', [
      'produk' => $produk,
      'kategori' => $kategori,
    ]);
  }

  function money($int) {
    return 'Rp '.number_format($int, 0, ',', '.');
  }

  function submit_produk(Request $request) {
    $id_produk = $request->input('id_produk');
    $nama = $request->input('nama');
    $id_kategori = $request->input('id_kategori');
    $harga = $request->input('harga');

    $data = [
      'id_kategori' => $id_kategori,
      'nama_produk' => $nama,
      'harga_produk' => $harga,
    ];
    if ($request->hasFile('image')) {
      $data['image'] = date('Y-m-d_His').'.'.$request->image->extension();
      $request->image->storeAs('img', $data['image']);
    }
    if ($id_produk == null) {
      DB::table('ms_produk')
        ->insert($data);
    } else {
      DB::table('ms_produk')
        ->where('id_produk', $id_produk)
        ->update($data);
    }

    if ($id_produk == null) {
      return redirect()->route('admin.produk')->with('messsage', 'Menu berhasil ditambah');
    } else {
      return redirect()->route('admin.produk')->with('messsage', 'Menu berhasil diperbarui');
    }
  }

  function hapus_produk(Request $request) {
    $id_produk = $request->input('id_produk');
    DB::table('ms_produk')
        ->where('id_produk', $id_produk)
        ->delete();
    return redirect()->route('admin.produk')->with('messsage', 'Menu berhasil dihapus');
  }

}
