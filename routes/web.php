<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');

Route::post('/submit_login', 'Auth\LoginController@authenticate')->name('auth.submit_login');

Route::prefix('admin')->group(function () {
  Route::middleware('auth')->group(function () {
    Route::prefix('')->namespace('Transaksi')->group(function() {
      Route::get('/transaksi', 'TransaksiController@page_transaksi')->name('admin.transaksi');
      Route::post('/transaksi/submit', 'TransaksiController@submit_transaksi');
      Route::get('/history', 'TransaksiController@page_history')->name('admin.history');
      Route::post('/detail_transaksi', 'TransaksiController@detail_transaksi')->name('admin.detail_transaksi');
      Route::post('/cari_menu', 'TransaksiController@cari_menu')->name('admin.cari_menu');
    });
    Route::prefix('')->namespace('Analisa')->group(function() {
      Route::get('/analisa', 'AnalisaController@page_analisa')->name('admin.analisa');
    });
    Route::prefix('produk')->namespace('Produk')->group(function() {
      Route::get('/', 'ProdukController@page_produk')->name('admin.produk');
      Route::get('/kategori', 'ProdukController@page_kategori')->name('admin.kategori');
      Route::post('/submit_produk', 'ProdukController@submit_produk')->name('admin.submit_produk');
      Route::post('/hapus_produk', 'ProdukController@hapus_produk')->name('admin.hapus_produk');
    });
  
    Route::prefix('user')->namespace('User')->group(function() {
      // Route::get('/', 'UserController@page_user')->name('admin.user');
      Route::get('/setting', 'UserController@setting')->name('admin.setting');
      Route::post('/update_setting', 'UserController@update_setting')->name('admin.update_setting');
      Route::get('/logout', 'UserController@logout')->name('admin.logout');
    });
  });
});
